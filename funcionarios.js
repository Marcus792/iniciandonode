const express = require('express');
const bodyParser = require('body-parser');
const req = require('express/lib/request'); 
const funcionario = express();
funcionario.use(bodyParser.urlencoded({extended:true}));
funcionario.use(bodyParser.json());
const port = 8082;

//habilitar o servidor 
funcionario.listen(port, ()=>{
    console.log("Projeto executando na porta " + port)
});

//recurso de request.query
funcionario.get('/funcionarios/filtros', (req, res)=>{
    let source = req.query;
    let ret = "Dados solicitados: " + source.nome;  
    res.send("{message:" + ret + "}");
});


//parametro no header chamado acess
//recurso de request.param
funcionario.delete('/funcionarios/pesquisa/:valor', (req,res)=>{
    let headers_ = req.headers["access"];
    if(headers_ == "access"){
        console.log("Valor Access: " + headers_);
        let dado = req.params.valor;
        let ret = "Dados deletados: " + dado;
        res.send("{message:" + ret +"}");
    }else{
        console.log ("Valor enviado no header não está correto");
        res.send("{message: Token inválido}");
    }
});



//put recurso de request.body 
funcionario.put('/funcionarios', (req,res)=>{
    //parametro header chamado access
    let headers_ = req.headers["access"];
    //validação
    if (headers_ == "access"){
        console.log("Valor Access: " + headers_);
        let dados = req.body;
        let ret = "Dados atualizados, Nome: " + dados.nome;
        res.send("{message:" +ret+"}");
    }else {
        console.log ("Valor enviado no header não está correto");
        res.send("{message: Token inválido}");
    }
});