//cliente (url: /clientes)
//get
//1º- parametro de envio via query (atributo nome)
//post
//criar um parametro no header chamado acess
//parametros de envio via body 
//validar se o valor que foi enviado no header está correto para prosseguir com a execução 
const express = require('express');
const bodyParser = require('body-parser');
const req = require('express/lib/request'); 
const cliente = express();
cliente.use(bodyParser.urlencoded({extended:true}));
cliente.use(bodyParser.json());
const port = 8081;

//habilitar o servidor 
cliente.listen(port, ()=>{
    console.log("Projeto executando na porta " + port)
});

//recurso de request.query
cliente.get('/clientes/filtros', (req, res)=>{
    let source = req.query;
    let ret = "Dados solicitados: " + source.nome;  
    res.send("{message:" + ret + "}");
});

//post request.body
cliente.post('/clientes', (req,res)=>{
    //parametro header chamado access
    let headers_ = req.headers["access"];
    //validação
    if (headers_ == "access"){
        console.log("Valor Access: " + headers_);
        let dados = req.body;
        let ret = "Dados enviados, Nome: " + dados.nome;
        res.send("{message:" +ret+"}");
    }else {
        console.log ("Valor enviado no header não está correto");
        res.send("{message: Token inválido}");
    }
});


